// express,mysql,axios,body-parser
const express = require("express");
const mysql = require("mysql2");
const bodyParser = require("body-parser");

// <script src="https://unpkg.com/axios/dist/axios.min.js"></script>

//import express,mysql,bodyparser

const app = express();
app.use(bodyParser.json());

//crearea variabilei de tip Express si parsarea in JSON

//legarea pe front


//deschiderea serverului pe portul 8080

app.listen(8080,()=>{
    console.log("Server running on 8080");
})

//crearea conexiunii la baza de date
const connection = mysql.createConnection({
    host:"localhost",
    user:"root",
    password:"",
    database:"cars"
})

// conectarea la BD si crearea unui table
connection.connect((error)=>{
    console.log("Connected to database!");
    const sql="CREATE TABLE IF NOT EXISTS cars(brand VARCHAR(25),hp INTEGER,color VARCHAR(15),capacity INTEGER)";
    connection.query(sql,(err)=>{
        if(err) console.log(err);
    })
})

// crearea unui obiect de tip masina pentru inserarea in baza de date printr-un post
app.post("/masina",(req,res)=>{

    const car = {
        brand:req.body.brand,
        hp:req.body.hp,
        color:req.body.color,
        capacity:req.body.capacity
    }
    let errors=[];


// validari pe cele 4 campuri

if(!car.brand||!car.color||!car.hp||!car.capacity){
    errors.push("Un camp nu este completat!");
}

// inserarea in baza de date a obiectului
if(errors.length==0){
    const sql = "INSERT INTO cars VALUES('"+car.brand+"',"+car.hp+",'"+car.color+"',"+car.capacity+")"
    connection.query(sql,(err)=>{
        if(err) throw err;
        else {
            console.log("Merge");
            res.status(200).send({
                message:"Ati introdus o masina!"
            })
        }
    })

}else{
    console.log("exista o eroare");
    res.status(500).send({
        message:"Eroare la server"
    })
}
})
